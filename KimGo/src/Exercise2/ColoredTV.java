package Exercise2;

public class ColoredTV extends tv {
	
	int brightness;
	int contrast;
	int picture;


	ColoredTV(String brand, String model) {
		super(brand, model);
		brightness = 50;
		contrast = 50;
		picture = 50;
		
	}
	
	void brightnessUp(){
		brightness++;
	}
	
	void brightnessDown(){
		brightness--;
	}
	
	void contrastUp(){
		contrast++;
	}
	
	void contrastDown(){
		contrast--;
	}
	
	void pictureUp(){
		picture++;
	}
	
	void pictureDown(){
		picture--;
	}
	
	void switchToChannel(int channel){
		this.channel = channel;
	}
	
	void mute(){
		volume = 0;
	}
	
	@Override
	public String toString() {
		return brand + " " +  model + "[" + "on:" + powerOn + ", " + "channel:" + channel + ", " + "volume" + volume + "]" + "[" + "b" + brightness + ", " + "c" + contrast +", " + "p" + picture + "]";
	
	}
	

	public static void main(String[] args) {
		tv bnwTV, sonyTV;
		bnwTV = new tv("Admiral", "A1");
		
		sonyTV = new ColoredTV("SONY", "S1");
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		
		ColoredTV sharpTV = new ColoredTV("SHARP","SH1");
		sharpTV.mute();
		sharpTV.brightnessUp();
		sharpTV.contrastDown();
		sharpTV.pictureUp();
		
		System.out.println(sharpTV);
	}
	
	

}
