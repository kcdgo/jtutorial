package Exercise2;

public class tv {

	protected String brand;
	protected String model;
	protected boolean powerOn;
	protected int channel;
	protected int volume;
	
	
	tv(String brand, String model){
		powerOn = false;
		channel = 0;
		volume = 5;
		this.brand = brand;
		this.model = model;
	}
	
	void turnOn(){
		powerOn = true;
	}
	
	void turnOff(){
		powerOn = false;
	}
	
	void channelUp(){
		channel++;
	}
	
	void channelDown(){
		channel--;
	}
	
	void volumeUp(){
		volume++;
	}
	
	void volumeDown(){
		volume--;
	}
	
	@Override
	public String toString() {

		return brand + " " +  model + "[" + "on:" + powerOn + ", " + "channel:" + channel + ", " + "volume" + volume + "]";
	}
	
	public static void main(String[] args) {
		tv tv1 = new tv("Andre Electronics","One");
		
		System.out.println(tv1);
		tv1.turnOn();
		tv1.channelUp();
		tv1.channelUp();
		tv1.channelUp();
		tv1.channelUp();
		tv1.channelUp();
		tv1.channelDown();
		tv1.volumeDown();
		tv1.volumeDown();
		tv1.volumeDown();
		tv1.volumeUp();
		tv1.turnOff();
		System.out.println(tv1);
		
		
	}
	
	
	
	
}
